<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$monthArr = array(
    '' => '===', 
    '01-' => 'Januari',
    '02-' => 'Februari',
    '03-' => 'Maret',
    '04-' => 'April',
    '05-' => 'Mei',
    '06-' => 'Juni',
    '07-' => 'Juli',
    '08-' => 'Agustus',
    '09-' => 'September',
    '10-' => 'Oktober',
    '11-' => 'November',
    '12-' => 'Desember');

$brnd = $this->db->select('name')
    ->from('brands')
    ->get()
    ->result();


$nowYear = date('Y');
$thn = "";
for ($i=0; $i <= 5 ; $i++) { 
    $thn .= "<option value='".($nowYear-$i)."'>".($nowYear-$i)."</option>";
}

$v = "";
/* if($this->input->post('name')){
  $v .= "&product=".$this->input->post('product');
  } */
if ($this->input->post('bulan') != '') {
    $v .= "&bulan=" . $this->input->post('bulan');
}
if ($this->input->post('tahun') != '') {
    $v .= "&tahun=" . $this->input->post('tahun');
}
if ($this->input->post('brand') != '') {
    $v .= "&brand=" . $this->input->post('brand');
}

?>
<script>
    $(document).ready(function () {
        oTable = $('#CusData').dataTable({
            "aaSorting": [[0, "asc"], [1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': false,
            'sAjaxSource': '<?= admin_url('reports/getCustomersBrand?v=1'.$v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [null, null, null, null, null]
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();
        <?php if ($this->input->post('customer')) { ?>
        $('#customer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });

        $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('customers_brand'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>"><i class="icon fa fa-file-excel-o"></i></a></li>
                <li class="dropdown"><a href="#" id="image" class="tip" title="<?= lang('save_image') ?>"><i class="icon fa fa-file-picture-o"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('view_report_customer'); ?></p>

                <div id="form">

                    <?php echo admin_form_open("reports/customers_brand"); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Bulan</label>
                                <select class="form-control" name="bulan">
                                    <?php foreach ($monthArr as $obj => $val) { ?>
                                        <option value="<?= $obj ?>"><?= $val ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control" name="tahun">
                                    <option value="">===</option>
                                    <?php echo $thn; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Brand</label>
                                <select class="form-control" name="brand">
                                    <option value="">===</option>
                                    <?php foreach ($brnd as $objbrand => $valbrand) { ?>
                                        <option value="<?= $valbrand->name ?>"><?= $valbrand->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div
                            class="controls"> <?php echo form_submit('submit_reports', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table id="CusData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped reports-table">
                        <thead>
                        <tr class="primary">
                            <th><?= lang("month") ?></th>
                            <th><?= lang("customer") ?></th>
                            <th><?= lang("customer_grand_total") ?></th>
                            <th><?= lang("customer_total_invoice") ?></th>
                            <th><?= lang("brand") ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reports/getCustomersBrand/pdf?v=1'.$v) ?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reports/getCustomersBrand/0/xls?v=1'.$v) ?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });
</script>