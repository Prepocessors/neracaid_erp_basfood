<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready( function() {
      initTable();
    });

    function initTable(){
    $('#cobaan').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': false, 'bServerSide': false,
            'sAjaxSource': '<?= admin_url('json_reseller/getJsonReseller') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>",
                    "class":  "<?= $this->security->get_csrf_token_name() ?>",
                });
                $.ajax({'dataType': 'json', 'type': 'get', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },            
            "aoColumnDefs" : [
                            {"aTargets" : [1], "sClass":  "field_1"}, 
                            {"aTargets" : [2], "sClass":  "field_2"},
                            {"aTargets" : [3], "sClass":  "field_3"},
                            {"aTargets" : [4], "sClass":  "field_4"},
                            {"aTargets" : [5], "sClass":  "field_5"},
                            {"aTargets" : [6], "sClass":  "field_6"},
                            {"aTargets" : [7], "sClass":  "field_7"},
                            {"aTargets" : [8], "sClass":  "field_8"},
                          ],
            "aoColumns": [null, null, null, null, null, null, null, null, null, null]
        });
    }

</script>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_biller'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("reseller/add", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <?= lang("logo", "biller_logo"); ?>
            <select class="form-control select" id="biller_logo" required="required" name="logo">
            <?php
            $biller_logos = array();
            foreach ($logos as $key => $value) { 
                array_push($biller_logos, $value) ?>
                <!-- // $biller_logos[$value] = $value; -->
                <option value="<?= $value ?>"><?= $value ?></option>
            <?php }
                // print_r($biller_logos['Neracaid.png']);
            // echo form_dropdown('logo', $biller_logos, '', 'class="form-control select" id="biller_logo" required="required" '); ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div id="logo-con" class="text-center">
            <img src="<?=base_url('assets/uploads/logos/').$biller_logos[0] ?>" alt="">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group company">
            <?= lang("company", "company"); ?>
            <?php echo form_input('company', 'Reseller Online Member', 'class="form-control tip" id="company" data-bv-notempty="true"'); ?>
        </div>
        <div class="form-group person">
            <?= lang("name", "name"); ?>
            <?php echo form_input('name', '', 'class="form-control tip" id="name" data-bv-notempty="true"'); ?>
        </div>
        <div class="form-group hilang">
            <?= lang("username","username") ?>
            <input type="text" name="username" class="form-control">
        </div>
        <div class="form-group hilang">
            <?= lang("password","password")." *" ?>
            <input type="password" name="password" id="password_one" class="form-control">
        </div>
        <div class="form-group hilang">
            <label>Verification</label>
            <input type="password" name="very_password" id="password_two" class="form-control">
            <span id="info_pass"></span>
        </div>
        <div class="form-group">
            <?= lang("email_address", "email_address"); ?>
            <input type="email" name="email" class="form-control" required="required" id="email_address"/>
        </div>
        <div class="form-group">
            <?= lang("phone", "phone"); ?>
            <input type="tel" name="phone" class="form-control" required="required" id="phone"/>
        </div>
        <div class="form-group">
            <?= lang("address", "address"); ?>
            <?php echo form_input('address', '', 'class="form-control" id="address" required="required"'); ?>
        </div>
        <div class="form-group">
            <?= lang("city", "city"); ?>
            <?php echo form_input('city', '', 'class="form-control" id="city" required="required"'); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= lang("state", "state"); ?>
            <?php
            if ($Settings->indian_gst) {
                $states = $this->gst->getIndianStates();
                echo form_dropdown('state', $states, '', 'class="form-control select" id="state" required="required"');
            } else {
                echo form_input('state', '-', 'class="form-control" id="state" required="required"');
            }
            ?>
        </div>
        <div class="form-group">
            <?= lang("postal_code", "postal_code"); ?>
            <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
        </div>
        <div class="form-group">
            <?= lang("country", "country"); ?>
            <?php echo form_input('country', '', 'class="form-control" id="country" required="required"'); ?>
        </div>
        <div class="form-group">
            <?= lang("gst_no", "gst_no"); ?>
            <?php echo form_input('gst_no', '', 'class="form-control" id="gst_no"'); ?>
        </div>
        <div class="form-group">
            <?= lang("vat_no", "vat_no"); ?>
            <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no"'); ?>
        </div>

        <!--<div class="form-group company">
            <?= lang("contact_person", "contact_person"); ?>
            <?php echo form_input('contact_person', '', 'class="form-control" id="contact_person" data-bv-notempty="true"'); ?>
        </div>-->
    </div>
</div>
<hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("bcf1", "cf1"); ?>
                        <?php
                        $push_bank[''] = '';
                        $arr_bank = array('mandiri' => "MANDIRI",'bca' => "BCA",'bni' => "BNI", 'Bank lainnya' => "Bank lainnya");
                        foreach ($arr_bank as $key => $value) {
                            $push_bank[$value] = $value;
                        }
                        echo form_dropdown('cf1', $push_bank, '', 'class="form-control select" id="cf1" required="required" '); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" name="bank_second" class="form-control" id="bank_second" placeholder="Bank lainnya..." disabled="">
                    </div>
                    <div class="form-group">
                        <?= lang("bcf2", "cf2"); ?>
                        <?php echo form_input('cf2', '', 'class="form-control" id="cf2" required="required"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf4", "cf4"); ?>
                        <?php echo form_input('cf4', '', 'class="form-control" id="cf4" required="required"'); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group all">
                        <label for="product_image">KTP</label>
<span class="file-input file-input-new">
<div class="input-group ">
   <input type="file" name="ktp_image">
</div>
</span>
                    </div>
                </div>
                <div class="col-md-6 hilang" >
                    <div class="form-group">
                        <?= lang("bcf3", "cf3"); ?>
                        <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf5", "cf5"); ?>
                        <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf6", "cf6"); ?>
                        <?php echo form_input('cf6', '', 'class="form-control" id="cf6"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("invoice_footer", "invoice_footer"); ?>
                        <?php echo form_textarea('invoice_footer', '', 'class="form-control skip" id="invoice_footer" style="height:100px;"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_biller', lang('add_biller'), 'class="btn btn-primary"'); ?>
        </div>

    <?php echo form_close(); ?>
    </div>
</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive" style="background-color: white">
                                <table id="cobaan" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr class="primary">
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Kota</th>
                                        <th>Alamat</th>
                                        <th>Email</th>
                                        <th>WA</th>
                                        <th>Bank</th>
                                        <th>No Rekening</th>
                                        <th>Attn</th>
                                        <th>KTP</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
            </div>  
        </div>
    </div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#biller_logo').change(function (event) {
            var biller_logo = $(this).val();
            $('#logo-con').html('<img src="<?=base_url('assets/uploads/logos')?>/' + biller_logo + '" alt="">');
        });

    $('#password_two').focusout(function(){
        checkpass();
    });
    function checkpass(){
        a = $('#password_one').val();
        b = $('#password_two').val();
        var html = '';
            if (a != b) {html += "<b style='color:red'> Password anda tidak sama, silakan cek kembali</b>";}
            $('#info_pass').html(html);   
        }
    $('#cobaan tbody').on( 'click', 'button', function () {
        var data0 = $(this).closest('tr').children('td.field_1').text(); // Nama
        var data1 = $(this).closest('tr').children('td.field_2').text(); // Kota
        var data2 = $(this).closest('tr').children('td.field_3').text(); // Alamat
        var data3 = $(this).closest('tr').children('td.field_4').text(); // Email
        var data4 = $(this).closest('tr').children('td.field_5').text(); // Wa
        var data5 = $(this).closest('tr').children('td.field_6').text(); // Bank
        var data6 = $(this).closest('tr').children('td.field_7').text(); // Norek
        var data7 = $(this).closest('tr').children('td.field_8').text(); // Attn
        $('#name').val(data0);
        $('#city').val(data1);
        $('#address').val(data2);
        $('#email_address').val(data3);
        $('#phone').val(data4);
        $('#cf1 option[value="'+data5+'"]').prop("selected", true);
        $('#cf2').val(data6);
        $('#cf4').val(data7);
    });

    $('#cf1').change(function(){
        var diff_s = $('div#s2id_cf1 > a > span.select2-chosen').text();
        // console.log(diff_s);
        if (diff_s == "Bank lainnya") {
            $('#bank_second').prop("disabled",false);
        }else{
            $('#bank_second').prop("disabled",true);
        }
    });

    });
</script>

<?= $modal_js ?>
