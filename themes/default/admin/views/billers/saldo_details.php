<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <div class="table-responsive">
                    <table id="getDetails" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th>ID</th>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('customer') ?></th>
                            <th><?= lang('grand_total') ?></th>
                            <th><?= lang('debit') ?></th>
                            <th><?= lang('kredit') ?></th>
                            <th><?= lang('description') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($dtls->result() as $key => $value) { ?>
                                <tr>
                                    <td><?= $value->id ?></td>
                                    <td><?= $value->tgl ?></td>
                                    <td><?= $value->customer ?></td>
                                    <td><?= $value->total ?></td>
                                    <td><?= $value->debit ?></td>
                                    <td><?= $value->kredit ?></td>
                                    <td><?= $value->keterangan ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>