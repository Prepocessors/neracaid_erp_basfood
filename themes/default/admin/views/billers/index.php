<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        oTable = $('#SupData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reseller/getBillers') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },  
            "aoColumnDefs" : [
                            {"aTargets" : [4], "sClass":  "field_4"},
                            {"aTargets" : [3], "sClass":  "field_3"},
                            {"aTargets" : [2], "sClass":  "field_2"},
                          ],
            "aaSorting": [[0,'desc']],
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, null, null, null, null, null, null, null, null, {"bSortable": false}]
        }).dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('company');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('vat_no');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('phone');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('city');?>]", filter_type: "text", data: []},
            {column_number: 7, filter_default_label: "[<?= 'Status';?>]", filter_type: "text", data: []},
        ], "footer");
    });
</script>
<?php if ($Owner || $GP['bulk_actions']) {
    echo admin_form_open('billers/biller_actions', 'id="action-form"');
} else{
    echo admin_form_open('reseller/biller_actions', 'id="action-form"');
}?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('billers'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?= admin_url('reseller/add'); ?>" data-toggle="modal" data-target="#myModal" id="add"><i class="fa fa-plus-circle"></i> <?= lang("add_biller"); ?></a></li>
                        <li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" class="bpo" title="<b><?= $this->lang->line("delete_billers") ?></b>" data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left"><i class="fa fa-trash-o"></i> <?= lang('delete_billers') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                    <table id="SupData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th><?= lang("company"); ?></th>
                            <th><?= lang("name"); ?></th>
                            <th><?= lang("phone"); ?></th>
                            <th><?= lang("email_address"); ?></th>
                            <th><?= lang("city"); ?></th>
                            <th><?= lang("bcf1"); ?></th>
                            <th><?= lang("bcf2"); ?></th>
                            <th>Status</th>
                            <th style="width:85px;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>



  <!-- Modal -->
  <div class="modal fade" id="MyGenerate" role="dialog">
    <?= admin_form_open('reseller/generateE',array('target'=>'_blank')) ?>
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-body">
            <input type="hidden" name="namaP" id="namaP">
            <input type="hidden" name="teleponP" id="teleponP">
            <input type="hidden" name="emailP" id="emailP">
            <h3 align="center">User <span class="nameP"></span> Belum mempunyai password, Silakan anda kabari via Whatsapp agar <span class="nameP"></span> bisa memasuki Reseller Online</h3>
        </div>
        
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Kirim via Whatsapp</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    <?= form_close() ?>
  </div>

<?php if ($action && $action == 'add') {
    echo '<script>$(document).ready(function(){$("#add").trigger("click");});</script>';
}
?>

<script>
    
    $('#SupData tbody').on( 'click', 'a.send-wa', function () {
        
        var data0 = $(this).closest('tr').children('td.field_3').text(); // Nama
        var data1 = $(this).closest('tr').children('td.field_2').text(); // Email
        var data2 = $(this).closest('tr').children('td.field_4').text(); // Email
        $('#teleponP').val(data0);
        $('#namaP').val(data1);
        $('#emailP').val(data2);

        $('.nameP').html(data1);
    });
</script>   

