<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
$max = str_replace(",", "", $this->input->get('saldo'));
?>
<script type="text/javascript">
    $("#datecredit").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                format: 'dd-mm-yyyy'
            }).datetimepicker('update', new Date());
</script>
<?= admin_form_open('reseller/getAddCredit'); ?>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title">Tambah Kredit (<?= $iden->name; ?>)</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" name="datecredit" class="form-control" id="datecredit">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Kredit</label>
                        <input type="text" name="credit" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="ketcredit" class="form-control">
                    </div>
                </div>
                <input type="hidden" name="idcredit" value="<?= $iden->id ?>">
                <input type="hidden" name="maxcredit" value="<?= $max ?>">
            </div>
        </div>
        <div class="modal-footer">
            <div class="buttons">
                <button type="submit" class="btn btn-info">Save</button>
            </div>
        </div>
    </div>
</div>
<?= form_close(); ?>