<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        var cTable = $('#CusData').dataTable({
            "aaSorting": [[0, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': false,
            'sAjaxSource': '<?= admin_url('reseller/getSaldoBillers') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            // "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            //         $('td', nRow).css('text-align', 'center');
            //         if ( aData[4] <= 30 )
            //         {
            //             $('td', nRow).css('background-color', 'rgb(255, 58, 58)');
            //         }
            //         else if ( aData[4] <= 60 )
            //         {
            //             $('td', nRow).css('background-color', 'rgb(255, 174, 58)');
            //         }
            //     },
            "aoColumns": [null, null, null, null, null, null, null, null],
            "aoColumnDefs": [
            {
            "mRender": function ( data, type, row ) {
                    // return data +' '+ row[6];
                    return "<div class='text-center'><div class='btn-group text-left'>"
                    +"<button type='button' class='btn btn-default btn-xs btn-primary dropdown-toggle' data-toggle='dropdown'>Aksi<span class='caret'></span></button>"
                    +"<ul class='dropdown-menu pull-right' role='menu'>"
                    +"<li><a href='<?= admin_url('reseller/saldoDetails/') ?>"+data+"' data-toggle='modal' data-target='#myModal'>View Details</a></li>"
                    +"<li><a href='<?= admin_url('reseller/addcredit/') ?>"+data+"?saldo="+row[6]+"' data-toggle='modal' data-target='#myModal'>Add Kredit</a></li>"
                    +"</ul>"
                    +"</div></div>";
                },
                "aTargets": [ 7 ],
            }
        ]
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('saldo_billers'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            
              <?= $this->session->flashdata('alertMax'); ?>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="CusData" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th><?= lang('name') ?></th>
                            <th><?= lang('bcf1') ?></th>
                            <th><?= lang('bcf2') ?></th>
                            <th><?= lang('bcf4') ?></th>
                            <th><?= lang('debit') ?></th>
                            <th><?= lang('kredit') ?></th>
                            <th>Saldo</th>
                            <th><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>