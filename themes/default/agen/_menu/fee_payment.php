<div class="tab-pane" id="tabs_2" role="tabpanel">
							<!-- BEGIN: Subheader -->
							<div class="m-subheader ">
								<div class="d-flex align-items-center">
									<div class="mr-auto">
										<h3 class="m-subheader__title ">Fee & Payments</h3>
									</div>
								</div>
							</div>

							<!-- END: Subheader -->
							<div class="m-content">
								<!--Begin::Section-->
								<div class="m-portlet">
									<div class="m-portlet__body  m-portlet__body--no-padding">
										<div class="row m-row--no-padding m-row--col-separator-xl">
											<div class="col-xl-12">

											<div class="col-xl-12">
												<div class="m-portlet__body">

													<!--begin: Search Form -->
													<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
														<div class="row align-items-center">
															<div class="col-xl-8 order-2 order-xl-1">
																<div class="form-group m-form__group row align-items-center">
																	<div class="col-md-4">
																		<div class="m-input-icon m-input-icon--left">
																			
														<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
																			<span class="m-input-icon__icon m-input-icon__icon--left">
																				<span><i class="la la-search"></i></span>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<!--end: Search Form -->
															
													<!--begin: Datatable -->
														<!-- <div class="col-sm-12" id="getFee"></div> -->
								<div class="m_datatable" id="local_data"></div>
													<!--end: Datatable -->
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	</div>