<div class="tab-pane <?= $stockes ?>" id="tabs_4" role="tabpanel">
							<!-- BEGIN: Subheader -->
							<div class="m-subheader ">
								<div class="d-flex align-items-center">
									<div class="mr-auto">
										<h3 class="m-subheader__title ">Check Stock</h3>
									</div>
								</div>
							</div>

							<!-- END: Subheader -->
							<div class="m-content">
								<!--Begin::Section-->
								<div class="m-portlet">
									<div class="m-portlet__body  m-portlet__body--no-padding">
										<div class="row m-row--no-padding m-row--col-separator-xl">
											<div class="col-xl-12">


												<div class="m-demo__preview">
													<div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
														<div class="form-group m-form__group">
															<div class="input-group">
																<input type="text" class="form-control" placeholder="Kode kategori" id="check_id_stock">
																<div class="input-group-append">
																	<button class="btn btn-warning" type="button" id="btn_check_1">Go!</button>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div>
	<div class="row" id="check_1"></div>
	
<div class="row" id="check_2">
<?php  
if ($this->input->get('codestock') != '') { 
$getStok = $getDataStock->result_array();
if (!empty($getStok) && is_array($getStok)) { ?>
		<div class="col-xl-6" style="margin:auto">
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">
				<div class="m-portlet__head m-portlet__head--fit">
				</div>
				<div class="m-portlet__body">
					<div class="m-widget19">
						<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">
							<img src="<?= base_url('assets/uploads/').$getStok[0]['image'] ?>" alt="" style="max-height:486px;object-fit: contain;">
								<!-- <h3 class="m-widget19__title m--font-light"> </h3> -->
								<div class="m-widget19__shadow"></div>
						</div>
						<div class="m-widget19__content">
							<div class="m-widget19__header">
								<div class="m-widget19__info">
									<span class="m-widget19__username"><?= $getStok[0]['name'] ?></span><br>
									<span class="m-widget19__time"><?= $getStok[0]['code'] ?></span>
								</div>
							</div>
							<div class="m-widget19__body"><?= $getStok[0]['description'] ?></div><p></p>
								<div class="table-responsive">
								<table class="table m-table m-table--head-bg-success table-bordered">
									<thead>
										<tr>
											<th>VARIAN</th>
											<th>QTY</th>
											<th>Harga</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($getDataStock->result() as $key => $value) { ?>
											<tr>
												<td><?= $value->variant ?></td>
												<td><?= $value->qty ?></td>
												<td>Rp <?= number_format($value->price, 0, ".", ".") ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
	<?php } else{ ?>
		<div class="col-xl-6" style="margin:auto">
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">
				<div class="m-portlet__head m-portlet__head--fit">
				</div>
				<div class="m-portlet__body">
					<div class="m-widget19">
						<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">
							<img src="<?= base_url('assets/uploads/avatars/Sorry.png') ?>" alt="" style="object-fit: contain;">
								<!-- <h3 class="m-widget19__title m--font-light"> </h3> -->
								<div class="m-widget19__shadow"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php }
	} ?>
</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
	</div>