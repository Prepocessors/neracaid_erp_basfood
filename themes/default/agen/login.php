<?php $this->load->view('default/agen/_base/header'); ?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?= base_url('styleMetronic/assets/app/media/img/logos/Nineswidosari_co.png') ?>" style="height: 150px">
									</a>
								</div>
								<div class="m-login__signin">
<!-- 									<div class="m-login__head">
										<h3 class="m-login__title">Sign In</h3>
									</div> -->
<?php
$attributes = array('class' => 'm-login__form m-form','novalidate' => 'novalidate');
echo form_open('onlinereseller/check_login',$attributes);
echo $this->session->flashdata('wrong');
?>
										<div class="form-group m-form__group has-danger">
											<input class="form-control m-input" type="email" placeholder="Email" name="identity" autocomplete="off" aria-describedby="email-error" value="<?= get_cookie('dsghajbvbxcayfgevwqbhjdhjksahn'); ?>" aria-invalid="true" required>
										</div>
										<div class="form-group m-form__group has-danger">
											<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" aria-describedby="password-error" value="<?= get_cookie('hdjsbvvewyuwqgvjdjxsakldjkbwnq') ?>" aria-invalid="true" required>
										</div>
										<div class="row m-login__form-sub">
											<div class="col m--align-left">
												<label class="m-checkbox m-checkbox--focus">
													<input type="checkbox" name="remember" <?php if (get_cookie('hdjsbvvewyuwqgvjdjxsakldjkbwnq')!='') { ?> checked <?php } ?> > Remember me
													<span></span>
												</label>
											</div>
											<div class="col m--align-right">
												<a href="<?= base_url('onlinereseller/faq') ?>" target="_blank" class="m-link">FAQ</a><br>
												<a href="http://bit.ly/Daftar-resellerNW" target="_blank" class="m-link">Daftar</a><br>
												<a href="javascript:;" id="m_login_forget_password" class="m-link">Lupa password ?</a>
											</div>
										</div>
										<div class="m-login__form-action">
											<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Login</button>
											<input type="submit" value="Login" class="hilang" id="check-login">
										</div>
										<input type="hidden" name="getCodeStock" value="<?= $this->input->get('getStock') ?>">
										<?= form_close(); ?>
									<!-- </form> -->
								</div>
								<div class="m-login__signup">
									<div class="m-login__head">
										<h3 class="m-login__title">Sign Up</h3>
										<div class="m-login__desc">Enter your details to create your account:</div>
									</div>
									<form class="m-login__form m-form" action="">
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Fullname" name="fullname">
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="password" placeholder="Password" name="password">
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="rpassword">
										</div>
										<div class="row form-group m-form__group m-login__form-sub">
											<div class="col m--align-left">
												<label class="m-checkbox m-checkbox--focus">
													<input type="checkbox" name="agree"> I Agree the <a href="#" class="m-link m-link--focus">terms and conditions</a>.
													<span></span>
												</label>
												<span class="m-form__help"></span>
											</div>
										</div>
										<div class="m-login__form-action">
											<button id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Sign Up</button>
											<button id="m_login_signup_cancel" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">Cancel</button>
										</div>
									</form>
								</div>
								<div class="m-login__forget-password">
									<div class="m-login__head">
										<h3 class="m-login__title">Forgotten Password ?</h3>
										<div class="m-login__desc">Enter your email to reset your password:</div>
									</div>
<?php  
$attributes = array('class' => 'm-login__form m-form');
echo form_open('onlinereseller/forgotPassword',$attributes);
?>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
										</div>
										<div class="m-login__form-action">
											<button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Request</button>
											<button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">Cancel</button>
											<input type="submit" value="Send" class="hilang" id="forgot-login">
										</div>
									<?= form_close(); ?>
								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--center">
							<!-- <div class="m-login__account">
								<span class="m-login__account-msg">
									Don't have an account yet ?
								</span>&nbsp;&nbsp;
								<a href="javascript:;" id="m_login_signup" class="m-link m-link--focus m-login__account-link">Sign Up</a>
							</div> -->
						</div>
					</div>
				</div>
				<!-- <div class="r-hidden"> -->
					<div class="r-hidden m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" style="background-image: url(<?= base_url('styleMetronic/assets/app/media/img//bg/bg-4.jpg') ?>)">
						<div class="m-grid__item">
							<h3 class="m-login__welcome" align="center">Selamat Datang Reseller Online</h3>
						</div>
					</div>
				<!-- </div> -->
			</div>
<?php $this->load->view('default/agen/_base/footer'); ?>