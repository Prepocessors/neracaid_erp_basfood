
		</div>
		<!-- end:: Page -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<!-- <script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script> -->
		
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<!-- <script src="<?= base_url() ?>styleMetronic/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script> -->
		<script src="<?= base_url() ?>styleMetronic/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<!-- <script src="<?= base_url() ?>styleMetronic/vendors/jstree/dist/jstree.js" type="text/javascript"></script> -->
		<script src="<?= base_url() ?>styleMetronic/vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="<?= base_url() ?>styleMetronic/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="<?= base_url() ?>styleMetronic/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>
		<!--begin::Page Scripts -->
		<script src="<?= base_url() ?>styleMetronic/assets/app/js/dashboard.js" type="text/javascript"></script>

		<!--begin::Page Scripts -->
		<script src="<?= base_url() ?>styleMetronic/assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>
		
		<!--end::Page Scripts -->
<!-- <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
		<!-- begin::Page Loader -->
		
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
		<script>
			$(window).on('load', function() {
				$('body').removeClass('m-page--loading');
			});
		</script>
		<!-- end::Page Loader -->
	</body>

	<!-- end::Body -->
</html>