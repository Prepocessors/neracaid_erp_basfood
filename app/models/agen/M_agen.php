<?php
class M_agen extends CI_Model
{
	
	function check_login($user,$pass)
	{
		$validate = $this->validate($user);
		$validate_email = $this->is_email_valid($user);

        $query = $this->db->get_where('companies', array($validate_email => $user, 'change_pass' => 1 ));
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();
            if (password_verify($pass, $validate) == true) {
	            $data = array(
	                    'name' => $row->name,
	                    'company' => $row->company,
	                    'email_agen' => $row->email,
	                    'logo' => $row->logo,
	                    'companies_id' => $row->id,
	                    'validated' => true
	                    );
	            $this->session->set_userdata($data);
	            return true;
			} else {
			    return false;
			}
        }
        // If the previous process did not validate
        // then return false.
        return false;
	}
	function is_email_valid($email) {
	    if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", trim($email))) {
	        return "email";
	    }
	    return "username";
	}
	function validate($user){
		$pass = "";
		$data = $this->db->select('password')
				->from('companies')
				->where('username',$user)
				->or_where('email',$user)
				->limit(1)
				->get()
				->result();
		foreach ($data as $key => $value) {
			$pass .= $value->password;
		}
		return $pass;
	}
	function update_pass($b,$c){
		$data = array(
		    'password' => password_hash($b, PASSWORD_BCRYPT),
			'change_pass' => 1
		);

		$this->db->where('email', $c);
		$this->db->update('companies', $data);
		return true;
	}
	function checkHelp($email = null){
		$data = $this->db->query("
			SELECT * 
FROM `sma_companies`
WHERE `sma_companies`.`group_name` = 'biller'
AND `sma_companies`.`company` = 'Reseller Online Member'
AND `sma_companies`.`password` IS NULL
AND `sma_companies`.`change_pass` = 0
AND `sma_companies`.`email` = '".$email."'
			");
		$row = $data->row();
		if ($data->num_rows() > 0) {
			return $row->email;
		}
		else{return false;}
	}
}
?>