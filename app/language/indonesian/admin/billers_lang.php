<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Billers
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_biller']                     = "Tambah Reseller";
$lang['edit_biller']                    = "Edit Reseller";
$lang['delete_biller']                  = "Hapus Reseller";
$lang['delete_billers']                 = "hapus Reseller";
$lang['biller_added']                   = "Reseller berhasil ditambahkan";
$lang['biller_updated']                 = "Reseller berhasil diperbarui";
$lang['biller_deleted']                 = "Reseller berhasil dihapus";
$lang['billers_deleted']                = "Reseller berhasil dihapus";
$lang['no_biller_selected']             = "Tidak ada penagih dipilih. Silakan pilih setidaknya satu tagihan.";
$lang['invoice_footer']                 = "Catatan kaki untuk Tagihan";
$lang['biller_x_deleted_have_sales']    = "Proses hapus gagal! Reseller memiliki data penjualan.";
$lang['billers_x_deleted_have_sales']   = "Beberapa Reseller tidak dapat dihapus karena mereka memiliki penjualan";