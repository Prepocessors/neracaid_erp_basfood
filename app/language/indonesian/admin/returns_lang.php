<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: SMS
 *
 * Last edited:
 * 9th November 2017
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['edit_return']                        = "Edit Pengembalian";
$lang['delete_return']                      = "hapus Pengembalian";
$lang['return_sale']                        = "Pengembalian Penjualan";
$lang['return_note']                        = "Return Note";
$lang['staff_note']                         = "Catatan Staf";
$lang['you_will_loss_return_data']          = "Anda akan kehilangan data pengembalian saat ini";
$lang['delete_returns']                     = "Hapus Pengembalian";
$lang['staff_note']                         = "Catatan Staf";
$lang['return_added']                       = "Pengembalian telah berhasil ditambahkan";
$lang['return_updated']                     = "Pengembalian telah berhasil diperbarui";
$lang['return_deleted']                     = "Pengembalian telah berhasil dihapus";
$lang['return_x_edited_older_than_x_days']  = "Pengembalian yang lebih tua dari %d hari tidak dapat diedit";
