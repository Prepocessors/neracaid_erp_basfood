<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Quotations
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_quote']                      = "Tambah Pesanan";
$lang['edit_quote']                     = "Edit Pesanan";
$lang['delete_quote']                   = "Hapus Pesanan";
$lang['delete_quotes']                  = "Hapus Pesanan";
$lang['quote_added']                    = "Pesanan berhasil ditambahkan";
$lang['quote_updated']                  = "Pesanan berhasil diperbarui";
$lang['quote_deleted']                  = "Pesanan berhasil dihapus";
$lang['quotes_deleted']                 = "Pesanan berhasil dihapus";
$lang['quote_details']                  = "Rincian Pesanan";
$lang['email_quote']                    = "Pesanan Email";
$lang['view_quote_details']             = "Lihat Rincian Penawaran";
$lang['quote_no']                       = "nomor Pesanan ";
$lang['send_email']                     = "Mengirim email";
$lang['quote_items']                    = "Items Pesanan";
$lang['no_quote_selected']              = "Tidak ada Pesanan yang dipilih. Silakan pilih setidaknya satu Pesanan.";
$lang['create_sale']                    = "Buat Penjualan";
$lang['create_purchase']                = "Buat Pembelian";
$lang['create_invoice']                 = "Buat Penjualan";