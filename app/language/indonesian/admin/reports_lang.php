<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Reports
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */


$lang['profit_estimate']            = "Perkiraan Untung";
$lang['warehouse_stock_heading']    = "Nilai Stok Gudang berdasarkan Biaya dan Harga. Silakan pilih gudang di sebelah kanan untuk mendapatkan nilai untuk gudang yang dipilih. ";
$lang['alert_quantity']             = "Peringatkan Kualitas";
$lang['customize_report']           = "Harap sesuaikan laporan di bawah ini";
$lang['start_date']                 = "Mulai tanggal";
$lang['end_date']                   = "Tanggal Berakhir";
$lang['purchased_amount']           = "Jumlah yang Dibeli";
$lang['sold_amount']                = "Jumlah Terjual";
$lang['profit_loss']                = "Untung dan / atau Rugi";      
$lang['daily_sales_report']         = "Laporan Penjualan Harian";
$lang['reports_calendar_text']      = "Anda dapat mengubah bulan dengan mengklik >> (berikutnya) atau << (sebelumnya)";
$lang['monthly_sales_report']       = "Laporan Penjualan Bulanan";
$lang['product_qty']                = "Produk (jumlah)";
$lang['payment_ref']                = "Referensi pembayaran";
$lang['sale_ref']                   = "Referensi Penjualan";
$lang['purchase_ref']               = "Referensi Pembelian";
$lang['paid_by']                    = "Dibayar oleh";
$lang['view_report']                = "Melihat laporan";
$lang['sales_amount']               = "Jumlah penjualan";
$lang['total_paid']                 = "Total Dibayar";
$lang['due_amount']                 = "Jumlah Karena";
$lang['total_sales']                = "Total Penjualan";
$lang['total_quotes']               = "Total Pesanan";
$lang['customer_sales_report']      = "Laporan Penjualan Pelanggan";
$lang['customer_payments_report']   = "Laporan Pembayaran Pelanggan";
$lang['purchases_amount']           = "Jumlah pembelian";
$lang['total_purchases']            = "Total Pembelian";
$lang['view_report_customer']       = "Silakan klik lihat laporan untuk memeriksa laporan pelanggan.";
$lang['view_report_supplier']       = "Silakan klik lihat laporan untuk memeriksa laporan pemasok.";
$lang['view_report_staff']          = "Silakan klik lihat laporan untuk memeriksa laporan staf.";
$lang['staff_purchases_report']     = "Laporan Pembelian Staf";
$lang['staff_sales_report']         = "Laporan Penjualan Staf";
$lang['staff_payments_report']      = "Laporan Pembayaran Staf";
$lang['group']                      = "Kelompok";
$lang['staff_daily_sales']          = "Penjualan Harian";
$lang['staff_monthly_sales']        = "Penjualan Bulanan";
$lang['staff_logins_report']        = "Laporan Login Staf";
$lang['add_customer']               = "Pelanggan";
$lang['show_form']                  = "Show Form";
$lang['hide_form']                  = "Sembunyikan Formulir ";
$lang['view_pl_report']             = "Silakan lihat laporan Laba dan / Rugi dan Anda dapat memilih rentang tanggal untuk menyesuaikan laporan. ";
$lang['payments_sent']              = "Pembayaran Terkirim";
$lang['payments_received']          = "Pembayaran diterima";
$lang['cheque']                     = "Memeriksa";
$lang['cash']                       = "uang tunai";
$lang['CC']                         = "Kartu kredit";
$lang['paypal_pro']                 = "Paypal Pro";
$lang['stripe']                     = "Garis";
$lang['cc_slips']                   = "CC Slips";
$lang['total_cash']                 = "Total uang tunai";
$lang['open_time']                  = "Waktu Terbuka";
$lang['close_time']                 = "waktu tertutup";
$lang['cash_in_hand']               = "Uang tunai di tangan";
$lang['Cheques']                    = "Cek";
$lang['save_image']                 = "Simpan sebagai Gambar";
$lang['total_quantity']             = "Jumlah total";
$lang['total_items']                = "Jumlah barang";
$lang['transfers_report']           = "Laporan Transfer";
$lang['transfer_no']                = "Nomor Transfer";
$lang['sales_return_report']        = "Laporan Pengembalian Penjualan";
$lang['payments_returned']          = "Pembayaran kembali";
$lang['category_code']              = "Kode Kategori";
$lang['category_name']              = "Nama Kategori";
$lang['total_returns']              = "Pengembalian Total";
$lang['stock_in_hand']              = "Stok (Jumlah) Amt";
$lang['expenses_report']            = "Laporan Biaya";
$lang['cheque_no']                  = "Periksa Nomor";
$lang['transaction_id']             = "ID transaksi";
$lang['card_no']                    = "Nomor Kartu (4 digit terakhir)";
$lang['net_sales']                  = "Penjualan bersih";"Subkategori";
$lang['net_purchases']              = "Pembelian Bersih";
$lang['subcategory']                = "Subkategori";
$lang['select_category_to_load']    = "Silakan pilih kategori yang akan dimuat";
$lang['select_subcategory']         = "Silakan pilih sub kategori";
$lang['no_subcategory']             = " Kategori tidak memiliki sub kategori";
$lang['daily_purchases_report']     = "Laporan Pembelian Harian";
$lang['monthly_purchases_report']   = "Laporan Pembelian Bulanan";
$lang['ref_no']                     = "nomor referensi";
$lang['ip_address']                 = "Alamat IP";
$lang['time']                       = "waktu";
$lang['deposits']                   = "Setoran";
$lang['sales_tax_report']           = "Laporan Pajak Penjualan";
$lang['purchases_tax_report']       = "Pembelian Laporan Pajak";
$lang['received_payment']           = "Pembayaran diterima";
$lang['total_product_tax']          = "Jumlah Pajak Produk";
$lang['total_order_tax']            = "Jumlah Pajak Pesanan";
$lang['refund']                     = "Pengembalian dana";
$lang['net_returns']                = "Pengembalian Bersih";
