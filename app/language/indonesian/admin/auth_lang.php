<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Auth
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['add_user']                                   = "Buat pengguna";
$lang['notify_user_by_email']                       = "Beri tahu Pengguna melalui Email";
$lang['group']                                      = "Group";
$lang['edit_user']                                  = "Edit Pengguna";
$lang['delete_user']                                = "Hapus pengguna";
$lang['user_added']                                 = "Pengguna berhasil ditambahkan";
$lang['user_updated']                               = "Pengguna berhasil diperbarui";
$lang['users_deleted']                              = "Pengguna berhasil dihapus";
$lang['alert_x_user']                               = "Anda akan menghapus pengguna ini secara permanen. Tekan OK untuk melanjutkan dan Batalkan untuk Kembali";
$lang['login_email']                                = "Email Login";
$lang['edit_profile']                               = "Edit Profil";
$lang['website']                                    = "Situs web";
$lang['if_you_need_to_rest_password_for_user']      = "Jika Anda perlu mengatur ulang kata sandi untuk pengguna ini.";
$lang['user_options']                               = "Opsi pengguna";
$lang['old_password']                               = "password lama";
$lang['new_password']                               = "kata sandi baru";
$lang['change_avatar']                              = "Ganti Avatar";
$lang['update_avatar']                              = "Perbarui Avatar";
$lang['avatar']                                     = "Avatar";
$lang['avatar_deleted']                             = "Avatar berhasil diperbarui";
$lang['captcha_wrong']                              = "Captcha salah. Silakan coba lagi";
$lang['captcha']                                    = "Captcha";
$lang['site_is_offline_plz_try_later']              = "Situs sedang offline. Kunjungi kami lagi dalam beberapa hari.";
$lang['type_captcha']                               = "Type Captcha";
$lang['we_are_sorry_as_this_sction_is_still_under_development'] = "Kami mohon maaf karena bagian ini masih dalam pengembangan dan kami mencoba membuatnya sesegera mungkin.";
$lang['confirm']                                    = "Konfirmasi";
$lang['error_csrf']                                 = "Pemalsuan permintaan lintas situs terdeteksi atau token csrf kedaluwarsa Silakan coba lagi.";
$lang['avatar_updated']                             = "Avatar berhasil diperbarui";
$lang['registration_is_disabled']                   = "Pendaftaran Akun ditutup.";
$lang['login_to_your_account']                      = "Silahkan masuk ke akun anda.";
$lang['pw']                                         = "Password";
$lang['remember_me']                                = "Remember me";
$lang['forgot_your_password']                       = "Lupa password?";
$lang['dont_worry']                                 = "Jangan khawatir!";
$lang['click_here']                                 = "klik disini";
$lang['to_rest']                                    = "untuk mengatur ulang";
$lang['forgot_password']                            = "Lupa Password";
$lang['login_successful']                           = "Anda berhasil masuk.";
$lang['back']                                       = "Kembali";
$lang['dont_have_account']                          = "Tidak punya akun?";
$lang['no_worry']                                   = "Jangan khawatir!";
$lang['to_register']                                = "mendaftar";
$lang['register_account_heading']                   = "Silakan isi formulir di bawah ini untuk mendaftar akun";
$lang['register_now']                               = "Daftar sekarang";
$lang['no_user_selected']                           = "Tidak ada pengguna yang dipilih. Silakan pilih setidaknya satu pengguna.";
$lang['delete_users']                               = "Hapus Pengguna";
$lang['delete_avatar']                              = "Hapus Avatar";
$lang['deactivate_heading']                         = "Apakah Anda yakin untuk menonaktifkan pengguna?";
$lang['deactivate']                                 = "Menonaktifkan";
$lang['pasword_hint']                               = "Setidaknya 1 huruf kapital, 1 huruf kecil, 1 angka, dan lebih dari 8 karakter";
$lang['pw_not_same']                                = "Kata sandi dan kata sandi konfirmasi tidak sama";
$lang['reset_password']                             = "Setel Ulang Kata Sandi";
$lang['reset_password_link_alt']                    = "Anda dapat menempelkan kode ini di bawah ini di url Anda jika tautan di atas tidak berfungsi";
$lang['email_forgotten_password_subject']           = "Setel Ulang Detail Kata Sandi";
$lang['reset_password_email']                       = "Setel ulang kata sandi untuk %s";
$lang['back_to_login']                              = "Kembali untuk masuk";
$lang['forgot_password_unsuccessful']               = "Gagal mengatur ulang kata sandi";
$lang['forgot_password_successful']                 = "Email telah dikirim dengan instruksi setel ulang kata sandi";
$lang['password_change_unsuccessful']               = "Perubahan kata sandi gagal";
$lang['password_change_successful']                 = "Kata sandi berhasil diubah";
$lang['forgot_password_email_not_found']            = "Masukkan alamat email bukan milik akun mana pun.";
$lang['login_unsuccessful']                         = "Info Masuk Gagal, Silakan coba lagi";
$lang['email_forgot_password_link']                 = "Setel Ulang Tautan Kata Sandi";
$lang['reset_password_heading']                     = "Setel Ulang Kata Sandi";
$lang['reset_password_new_password_label']          = "kata sandi baru";
$lang['reset_password_new_password_confirm_label']  = "Konfirmasi password baru";
$lang['register']                                   = "Daftar";
$lang['email_new_password_subject']                 = "Kata sandi akun telah disetel ulang";
$lang['reset_password_submit_btn']                  = "Setel Ulang Kata Sandi";
$lang['error_csrf']                                 = 'Posting formulir ini tidak lulus pemeriksaan keamanan kami.';
$lang['account_creation_successful']                = "Akun berhasil dibuat";
$lang['old_password_wrong']                         = "Silakan ketik kata sandi lama yang benar";
$lang['sending_email_failed']                       = "Tidak dapat mengirim email, harap periksa pengaturan sistem Anda.";
$lang['deactivate_successful']                      = "Pengguna berhasil dinonaktifkan";
$lang['activate_successful']                        = "Pengguna berhasil diaktifkan";
$lang['login_timeout']                              = "Anda memiliki 3 upaya login yang gagal. Silakan coba setelah 10 menit";
$lang['view_right']                                 = "View rights";
$lang['edit_right']                                 = "Edit rights";
$lang['all_records']                                = "Semua catatan";
$lang['own_records']                                = "Catatan sendiri";
$lang['allow_discount']                             = "Izinkan Diskon";
$lang['new_user_created']                           = "Pengguna Baru Dibuat";
$lang['type_email_to_reset']                        = "Silakan ketik alamat email untuk mendapatkan instruksi reset";
$lang['account_exists']                             = "Akun sudah ada, silakan coba dengan nama pengguna / email ini atau setel ulang kata sandi Anda. ";
