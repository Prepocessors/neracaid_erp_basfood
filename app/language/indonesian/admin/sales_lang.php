<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Sales
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */

$lang['add_sale']                           = "Tambah Penjualan ";
$lang['stockis']                            = "Stockis";
$lang['all_customer']						= "Semua";
$lang['add_sale_stockis']                   = "Tambah Penjualan (Stockis)";
$lang['edit_sale']                          = "Edit Penjualan";
$lang['delete_sale']                        = "Hapus Penjualan";
$lang['delete_sales']                       = "Hapus Penjualan";
$lang['sale_added']                         = "Penjualan berhasil ditambahkan";
$lang['sale_updated']                       = "Penjualan berhasil diperbarui";
$lang['sale_deleted']                       = "Penjualan berhasil dihapus";
$lang['sales_deleted']                      = "Penjualan berhasil dihapus";
$lang['incorrect_gift_card']                = "Nomor kartu hadiah salah atau kedaluwarsa.";
$lang['gift_card_not_for_customer']         = "Nomor kartu hadiah bukan untuk pelanggan ini.";
$lang['view_payments']                      = "Lihat Pembayaran";
$lang['add_payment']                        = "Tambahkan Pembayaran";
$lang['add_delivery']                       = "Tambahkan Pengiriman";
$lang['email_sale']                         = "Penjualan Email";
$lang['return_sale']                        = "Pengembalian Penjualan";
$lang['sale_status']                        = "Status Penjualan";
$lang['payment_term']                       = "Jangka waktu pembayaran";
$lang['payment_term_tip']                   = "Harap ketik jumlah hari saja (bilangan bulat) saja";
$lang['sale_note']                          = "Catatan Penjualan";
$lang['staff_note']                         = "Catatan Staf";
$lang['serial_no']                          = "Nomor seri";
$lang['product_option']                     = "Opsi Produk";
$lang['product_serial']                     = "Serial produk";
$lang['list_sale']                          = "Daftar Penjualan";
$lang['do_reference_no']                    = "Referensi Pengiriman Nomer.";
$lang['sale_reference_no']                  = "Referensi nomer penjualan.";
$lang['edit_delivery']                      = "Edit Pengiriman";
$lang['delete_delivery']                    = "Hapus Pengiriman";
$lang['delete_deliveries']                  = "Hapus Pengiriman";
$lang['delivery_added']                     = "Pengiriman berhasil ditambahkan";
$lang['delivery_updated']                   = "Pengiriman berhasil diperbarui";
$lang['delivery_deleted']                   = "Pengiriman berhasil dihapus";
$lang['deliveries_deleted']                 = "Pengiriman berhasil dihapus";
$lang['email_delivery']                     = "Pengiriman Email";
$lang['delivery_details']                   = "Rincian pengiriman";
$lang['delivery_order']                     = "Pesan antar";
$lang['product_details']                    = "Rincian Produk";
$lang['prepared_by']                        = "Disiapkan oleh";
$lang['delivered_by']                       = "Dikirim oleh";
$lang['received_by']                        = "Diterima oleh";
$lang['edit_payment']                       = "Edit Pembayaran";
$lang['delete_payment']                     = "Hapus Pembayaran";
$lang['delete_payments']                    = "Hapus Pembayaran";
$lang['payment_added']                      = "Pembayaran berhasil ditambahkan";
$lang['payment_updated']                    = "Pembayaran berhasil diperbarui";
$lang['payment_deleted']                    = "Pembayaran berhasil dihapus";
$lang['payments_deleted']                   = "Pembayaran berhasil dihapus";
$lang['paid_by']                            = "Dibayar oleh";
$lang['send_email']                         = "Mengirim email";
$lang['add_gift_card']                      = "Tambahkan Kartu Hadiah";
$lang['edit_gift_card']                     = "Edit Kartu Hadiah";
$lang['delete_gift_card']                   = "Hapus Kartu Hadiah";
$lang['delete_gift_cards']                  = "Hapus Kartu Hadiah";
$lang['gift_card_added']                    = "Kartu Hadiah berhasil ditambahkan";
$lang['gift_card_updated']                  = "Kartu Hadiah berhasil diperbarui";
$lang['gift_card_deleted']                  = "Kartu Hadiah berhasil dihapus";
$lang['gift_cards_deleted']                 = "Kartu Hadiah berhasil dihapus";
$lang['card_no']                            = "nomorkartu";
$lang['balance']                            = "saldo";
$lang['value']                              = "Nilai";
$lang['new_gift_card']                      = "Tambahkan Kartu Hadiah";
$lang['sell_gift_card']                     = "Jual Kartu Hadiah";
$lang['view_sales_details']                 = "Lihat Rincian Penjualan";
$lang['sale_no']                            = "Nomor Penjualan";
$lang['payment_reference']                  = "Referensi pembayaran";
$lang['quantity_out_of_stock_for_%s']       = "Kuantitas habis untuk %s";
$lang['no_sale_selected']                   = "Tidak ada penjualan yang dipilih. Silakan pilih setidaknya satu penjualan.";
$lang['you_will_loss_sale_data']            = "Anda akan kehilangan data penjualan saat ini. Apakah Anda ingin melanjutkan?";
$lang['sale_status_x_competed']             = "Status penjualan sedang menunggu, Anda dapat menambahkan penjualan kembali hanya untuk penjualan yang selesai. ";
$lang['unselect_customer']                  = "Hapus Pelanggan";
$lang['payment_status_not_paid']            = "Harap diperhatikan bahwa status pembayaran penjualan ini tidak dibayarkan.";
$lang['paid_amount']                        = "Jumlah pembayaran";
$lang['surcharges']                         = "Biaya tambahan";
$lang['return_surcharge']                   = "Pengembalian Biaya";
$lang['return_amount']                      = "Jumlah Pengembalian";
$lang['sale_reference']                     = "Referensi Penjualan";
$lang['return_sale_no']                     = "Penjualan Kembali Nomor";
$lang['view_return']                        = "Lihat Kembali";
$lang['return_sale_deleted']                = "Penjualan kembali berhasil dihapus";
$lang['total_before_return']                = "Total Sebelum Kembali";
$lang['return_amount']                      = "Jumlah Pengembalian";
$lang['return_items']                       = "mengembalikan barang";
$lang['surcharge']                          = "Biaya tambahan";
$lang['returned_items']                     = "barang yang dikembalikan";
$lang['seller']                             = "Penjual";
$lang['users']                              = "Pengguna";
$lang['return_note']                        = "Return Note";
$lang['return_sale_added']                  = "Penjualan pengembalian berhasil ditambahkan";
$lang['return_has_been_added']              = "Beberapa item telah dikembalikan untuk penjualan ini";
$lang['return_surcharge']                   = "Pengembalian Biaya";
$lang['payment_returned']                   = "Pembayaran Dikembalikan";
$lang['payment_received']                   = "Pembayaran diterima";
$lang['payment_note']                       = "Catatan Pembayaran";
$lang['view_return_details']                = "Lihat Detail Pengembalian";
$lang['pay_by_paypal']                      = "Bayar dengan Paypal";
$lang['pay_by_skrill']                      = "Bayar dengan Skrill";
$lang['tax_invoice']                        = "PPN";
$lang['sale_x_edited_older_than_x_days']    = "Penjualan tidak dapat diedit karena penjualan ini lebih dari %d hari.";
$lang['use_award_points']                   = "Gunakan Poin Penghargaan";
$lang['use_staff_award_points']             = "Gunakan Poin Penghargaan Staf";
$lang['use_points']                         = "Gunakan Poin";
$lang['award_points_wrong']                 = "Silakan periksa kolom input penggunaan poin, nilainya salah.";
$lang['tax_rate_name']                      = "Nama Tarif Pajak";
$lang['auto_added_for_sale_by_csv']         = "Secara otomatis ditambahkan untuk dijual oleh csv";
$lang['invoice']                            = "Invoice";
$lang['view_gift_card']                     = "Lihat Kartu Hadiah";
$lang['card_expired']                       = "Kartu hadiah sudah kedaluwarsa";
$lang['card_is_used']                       = "Kartu hadiah kami sudah digunakan";
$lang['no_delivery_selected']               = "Tidak ada pesanan pengiriman yang dipilih. Harap pilih setidaknya satu pesanan.";
$lang['sale_not_found']                     = "Penjualan tidak ditemukan.";
$lang['x_edit_payment']                     = "Pembayaran tidak dapat diedit.";
$lang['transaction_id']                     = "ID transaksi";
$lang['return_quantity']                    = "Kuantitas yang dikembalikan";
$lang['return_tip']                         = "Harap edit jumlah pengembalian di bawah. Anda dapat menghapus item atau mengatur kuantitas pengembalian ke nol jika tidak dikembalikan ";
$lang['sale_x_action']                      = "Tindakan ini tidak dapat dilakukan untuk dijual dengan catatan pengembalian";
$lang['sale_already_returned']              = "Dijual sudah memiliki catatan pengembalian";
$lang['sale_is_returned']                   = "Penjualan memiliki catatan pengembalian";
$lang['payment_was_returned']               = "Pembayaran telah dikembalikan";
$lang['packing']                            = "Pengepakan";
$lang['delivering']                         = "Sedang dikirim";
$lang['delivered']                          = "Terkirim";
$lang['status_is_x_completed']              = "Status penjualan tidak lengkap, Anda dapat menambahkan pesanan pengiriman hanya untuk penjualan selesai ";
$lang['delivery_already_added']             = "Pesanan pengiriman sudah ada untuk penjualan";
$lang['sale_already_paid']                  = "Status pembayaran sudah dibayar untuk penjualan";
$lang['topup_gift_card']                    = "Kartu Hadiah Topup ";
$lang['last_topups']                        = "Topup Terakhir";
$lang['topup_added']                        = "Saldo kartu hadiah berhasil diperbarui";
$lang['duplicate_sale']                     = "Duplikasi penjualan";
$lang['update_customer_email']              = "Harap perbarui alamat email pelanggan";
