<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Transfers
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You alseo can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_transfer']                                   = "Tambahkan Transfer";
$lang['edit_transfer']                                  = "Edit Transfer";
$lang['delete_transfer']                                = "Hapus Transfer";
$lang['delete_transfers']                               = "Hapus Transfer";
$lang['transfer_added']                                 = "Transfer berhasil ditambahkan";
$lang['transfer_updated']                               = "Transfer berhasil diperbarui";
$lang['transfer_deleted']                               = "Transfer berhasil dihapus";
$lang['transfers_deleted']                              = "Transfer berhasil dihapus";
$lang['import_by_csv']                                  = "Tambahkan Transfer oleh CSV";
$lang['ref_no']                                         = "nomor referensi";
$lang['transfer_details']                               = "Rincian Transfer";
$lang['email_transfer']                                 = "Email Transfer";
$lang['transfer_quantity']                              = "Jumlah Transfer";
$lang['please_select_warehouse']                        = "Silakan pilih gudang";
$lang['please_select_different_warehouse']              = "Silakan pilih gudang yang berbeda";
$lang['to_warehouse']                                   = "Ke Gudang";
$lang['from_warehouse']                                 = "Dari Gudang";
$lang['edit_transfer_quantity']                         = "Edit Kuantitas Transfer";
$lang['can_not_change_status_of_completed_transfer']    = "Anda tidak dapat mengubah status transfer yang selesai";
$lang['transfer_by_csv']                                = "Tambahkan Transfer oleh CSV";
$lang['no_transfer_selected']                           = "Tidak ada transfer yang dipilih. Silakan pilih setidaknya satu transfer.";
$lang['received_by']                                    = "Diterima oleh";
$lang['users']                                          = "Users";
$lang['transferring']                                   = "Mentransfer";
$lang['first_2_are_required_other_optional']            = "<strong> Dua kolom pertama diperlukan dan yang lainnya opsional. </strong>";
$lang['pr_not_found']                                   = "Tidak ada produk yang ditemukan ";
$lang['line_no']                                        = "Nomor Baris";
$lang['transfer_order']                                 = "Pesanan Transfer";
$lang['unit_cost']                                      = "Biaya Unit";
