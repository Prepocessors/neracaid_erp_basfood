<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'third_party/GetJsonReseller/vendor/autoload.php');

class Json_Reseller extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer && $this->Sales) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('billers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('json_reseller_model');
    }
    
    function getClient(){
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
    $client->setAuthConfig(APPPATH.'third_party/GetJsonReseller/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = APPPATH.'third_party/GetJsonReseller/token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        // Save the token to a file.
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


    function getJsonReseller(){
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        // Prints the names and majors of students in a sample spreadsheet:
        // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
        $spreadsheetId = '1SNpO_fAVhELeZQGdkysc7YDlBNq7e_jSDUzabD55Hso';
        $range = 'Form Responses 1!A2:N';
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();

        if (empty($values)) {
            print "No data found.\n";
        } else {
            $arr= array();
            $no=1;
            foreach ($values as $row) {
                $check_user = $this->json_reseller_model->check_user($row[2]);
                if ($check_user) {           
                    $ktp = "<a href='".$row[6]."' target='_blank'>Foto KTP</a>";
                    $copy = "<button class='btn btn-info copy'>Copy data</button>";
                    $data = array($copy,$row[2],$row[8],$row[3],$row[1],$row[4],$row[7],$row[5],$row[9],$ktp);
                    // $x = array(
                    //     "data" => $data
                    // );
                    array_push($arr, $data);
                }
                // Print columns A and E, which correspond to indices 0 and 4.
                // printf("%s,%s", $row[0], $row[6]);
              // print_r($row[1]);

                // $data['time']       = $row[0];
                // $data['email']      = $row[1];
                // $data['nama']       = $row[2];
                // $data['alamat']     = $row[3];
                // $data['no_wa']      = $row[4];
                // $data['norek']      = $row[5];
                // $data['link_ktp']   = $row[6];
                // $data['bank']   = $row[7];
                // $data['kota']   = $row[8];
                // $x = array(
                //     "data" => $data
                // );
                // array_push($arr, $data);
            }
            $array_fix = array(
                "sEcho" => intval($this->input->post('sEcho')),
                "iTotalRecords" => count($arr),
                "iTotalDisplayRecords" => count($arr),
                "aaData" => $arr,
                "sColumns" => "no,time,nama,alamat,email,no_wa,bank,norek,attn,link_ktp"
            );
            // print_r($arr);
            echo json_encode($array_fix);
        }
    }

    function index(){
        $this->load->view('default/agen/tampilReseller');
    }
}

?>