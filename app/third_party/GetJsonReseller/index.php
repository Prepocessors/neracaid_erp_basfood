<!DOCTYPE html>
<html lang="en">
<head>
  <title>API Spreadsheet Google</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

<div class="container">
  <h2>Bordered Table</h2>
  <p>The .table-bordered class adds borders to a table:</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Time</th>
        <th>Email</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>No_wa</th>
        <th>Bank</th>
        <th>No Rek</th>
        <th>Link KTP</th>
      </tr>
    </thead>
    <tbody id="showdata">

    </tbody>
  </table>
</div>
<script src="jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
        function tampilData(){
            $.ajax({
                type : 'ajax',
                url : 'http://localhost/buatbaru/get_datanya.php',
                async : false,
                dataType :'json',
                success : function(data){
                    // console.log(data[0].nama);
                    var html = '';
                    var i;
                    for (i=0 ; i<data.length; i++) {
                        html += "<tr>"+
                            "<td>"+data[i].time+"</td>"+
                            "<td>"+data[i].email+"</td>"+
                            "<td>"+data[i].nama+"</td>"+
                            "<td>"+data[i].alamat+"</td>"+
                            "<td>"+data[i].no_wa+"</td>"+
                            "<td>"+data[i].bank+"</td>"+
                            "<td>"+data[i].norek+"</td>"+
                            "<td>"+data[i].link_ktp+"</td>"+
                            "</tr>";
                    }
                    $("#showdata").html(html);
                },
                error : function(){
                    // alert("Data tidak ada");
                }
            });
        }
        function myFunction() {
          myVar = setInterval(tampilData, 2000);
        }
    $(document).ready(function(){
        // tampilData();
        myFunction();
    });
</script>
</body>
</html>